# trap_print

a helper script for displaying output from a script, making sure it exits with an error on error in script.

## problem

sample build script:

```bash
#!/bin/bash
export mydestdir=$PWD/install
./autogen.sh
./configure
make
make DESTDIR=$mydestdir install
```

well:

1.  this won't work as per default the `set +e` is not set, causing the script not to exit on an error.
2.  only the output is displayed, resulting in a lot of info without knowing where it comes from
3. if we would add `set +x`, we can see what's going on, but it's very messy
4.  `make` produces a lot of output, that if things go correctly has no added value to display in CI, clogging up the stdout and bluring posible stderr messages that have meaning.

## Solution

we source `trap_print.sh` for our demo project and create `make_it.sh` as:

```bash
#!/bin/bash
. ./trap_print.sh
export mydestdir=$PWD/install
autoreconf -ifv . >/dev/null
./configure >/dev/null
make >/dev/null
make DESTDIR=$mydestdir install >/dev/null
f_exit
```

Sample Result:

![](trap_output.png)

## commands

|command|description|
|--|------|
| f_verbose_command | turn verbose command display on |
| f_quiet_command | turn verbose command display off  |
| f_fail | exit with fail |
| f_exit | exit script with last command exit code |

## suppress stdout and/or stderr

while it's not always opportune to display all info sent to stdout or stderr from a script, we can redirect it to `/dev/null`, doing so trap_print will inform you that the output is being suppressed.

## Usage

### Download latest:

```bash
curl -O https://trap-print.tools.odagrun.com/latest/files/trap_print
```

### use

```bash
. path-to-trap_print
...
```

or 

```bash
source path-to-trap_print
...
```

### for use without storing trap_print:

```
#!/bin/bash
source /dev/stdin <<< "$(curl -s https://trap-print.tools.odagrun.com/latest/files/trap_print)"
<my command>
....
```
